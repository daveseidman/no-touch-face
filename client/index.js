import * as facemesh from '@tensorflow-models/facemesh';
import * as tf from '@tensorflow/tfjs-core';
import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import { version } from '@tensorflow/tfjs-backend-wasm/dist/version';

tfjsWasm.setWasmPath(`https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${version}/dist/tfjs-backend-wasm.wasm`);

let model;
let ctx;
let videoWidth;
let videoHeight;
let video;
let canvas;

const colorsOverTime = [];
const TIME_SPAN = 5;
const features = [
  { id: 1, color: { r: 0, g: 0, b: 0 } },
  { id: 205, color: { r: 0, g: 0, b: 0 } },
  { id: 425, color: { r: 0, g: 0, b: 0 } },
  { id: 168, color: { r: 0, g: 0, b: 0 } },
];

async function setupCamera() {
  video = document.getElementById('video');

  const stream = await navigator.mediaDevices.getUserMedia({
    audio: false,
    video: {
      facingMode: 'user',
      width: 640,
      height: 480,
    },
  });
  video.srcObject = stream;

  return new Promise((resolve) => {
    video.onloadedmetadata = () => {
      resolve(video);
    };
  });
}

async function renderPrediction() {
  const predictions = await model.estimateFaces(video);
  ctx.drawImage(
    video, 0, 0, videoWidth, videoHeight, 0, 0, canvas.width, canvas.height,
  );

  if (predictions.length > 0) {
    predictions.forEach((prediction) => {
      const keypoints = prediction.scaledMesh;
      const colors = [];

      for (let i = 0; i < features.length; i += 1) {
        const x = keypoints[features[i].id][0];
        const y = keypoints[features[i].id][1];

        ctx.beginPath();
        ctx.arc(x, y, 3, 0, 2 * Math.PI);
        ctx.fill();

        const imgData = ctx.getImageData(x - 1, y - 1, 3, 3).data;
        const red = (imgData[0] + imgData[4] + imgData[8] + imgData[12] + imgData[16] + imgData[20] + imgData[24] + imgData[28] + imgData[32]) / 9;
        const grn = (imgData[1] + imgData[5] + imgData[9] + imgData[13] + imgData[17] + imgData[21] + imgData[25] + imgData[29] + imgData[33]) / 9;
        const blu = (imgData[2] + imgData[6] + imgData[10] + imgData[14] + imgData[18] + imgData[22] + imgData[26] + imgData[30] + imgData[34]) / 9;

        colors.push({ feature: features[i], pos: { x, y }, colors: { red, grn, blu } });
      }

      colorsOverTime.push(colors);
    });

    if (colorsOverTime.length > TIME_SPAN) {
      colorsOverTime.shift();
      // console.log(colorsOverTime);
      for (let f = 0; f < features.length; f += 1) {
        for (let i = 0; i < TIME_SPAN; i += 1) {
          features[f].color.r += colorsOverTime[i][f].colors.red;
          features[f].color.g += colorsOverTime[i][f].colors.grn;
          features[f].color.b += colorsOverTime[i][f].colors.blu;
        }
        features[f].color.r /= TIME_SPAN;
        features[f].color.g /= TIME_SPAN;
        features[f].color.b /= TIME_SPAN;
        features[f].pos = colorsOverTime[TIME_SPAN - 1][f].pos;

        ctx.fillText(`${Math.round(features[f].color.r)}, ${Math.round(features[f].color.g)}, ${Math.round(features[f].color.b)}`, features[f].pos.x, features[f].pos.y);
      }
    }
  }


  requestAnimationFrame(renderPrediction);
}

async function main() {
  await tf.setBackend('wasm');

  await setupCamera();
  video.play();
  videoWidth = video.videoWidth;
  videoHeight = video.videoHeight;
  video.width = videoWidth;
  video.height = videoHeight;

  canvas = document.getElementById('output');
  canvas.width = videoWidth;
  canvas.height = videoHeight;
  const canvasContainer = document.querySelector('.canvas-wrapper');
  canvasContainer.style = `width: ${videoWidth}px; height: ${videoHeight}px`;

  ctx = canvas.getContext('2d');
  ctx.translate(canvas.width, 0);
  ctx.scale(-1, 1);
  ctx.font = '12px sans-serif';
  ctx.fillStyle = '#32EEDB';
  ctx.strokeStyle = '#32EEDB';
  ctx.lineWidth = 0.5;

  model = await facemesh.load({ maxFaces: 1 });
  renderPrediction();
}


main();
