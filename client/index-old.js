import * as facemesh from '@tensorflow-models/facemesh';
import './index.scss';

const maxFaces = 1;

// model = facemesh.load({ maxFaces }).then((res) => {
facemesh.load({ maxFaces }).then((model) => {
  // model2 = res;
  const video = document.createElement('video');
  video.className = 'video';
  document.body.appendChild(video);
  video.autoPlay = true;
  video.mute = true;
  const constraints = { video: true };

  const getCamera = () => {
    video.removeEventListener('click', getCamera);
    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      video.srcObject = stream;
      video.play();

      // video.addEventListener('loadeddata', beginTracking);
      video.addEventListener('loadeddata', () => {
        model.estimateFaces(video).then((res) => {
          console.log(res);
        });
      });
      // main();
    });
  };

  video.addEventListener('click', getCamera);
});
